/*
 * *
 *  * Created by Mugundhan on 4/26/22, 6:44 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 4/25/22, 3:20 PM
 *
 */

package com.basepackmvvm.network


import com.basepackmvvm.data.model.response.NetworkUserListItem
import com.basepackmvvm.data.model.response.SampleUser
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("users")
    suspend fun getUserList(@Query("page") page:String): SampleUser
}