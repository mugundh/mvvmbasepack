/*
 * Created by Savitha on 12/10/21, 1:20 PM
 * Copyright (c) 2021 . All rights reserved.
 * Last modified 12/10/21, 1:19 PM
 */

package com.basepackmvvm.network


import com.basepackmvvm.data.model.response.BaseErrorResponse
import com.squareup.moshi.Moshi
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException
import javax.inject.Inject

enum class ErrorCodes(val code: Int) {
    SocketTimeOut(-1)
}

open class SafeApiRequest
@Inject constructor(
    private val networkHelper: NetworkHelper
) {
    suspend fun <T : Any> apiRequest(dataRequest: suspend () -> T): ApiState<T> {
        return try {
            if (networkHelper.isNetworkConnected()) {
                ApiState.Success(dataRequest.invoke())
            } else
                throw NoInternetException("Please check your Internet Connection")
        } catch (throwable: Throwable) {
            when (throwable) {
                is IOException -> ApiState.GenericError(
                    throwable.message,
                    null
                )
                is HttpException -> {
                    val code = throwable.code()
                    val errorResponse = convertErrorBody(throwable)
                    ApiState.GenericError(getErrorMessage(code), errorResponse)
                }
                is SocketTimeoutException -> ApiState.NetworkError(
                    throwable,
                    getErrorMessage(ErrorCodes.SocketTimeOut.code)
                )
                is NoInternetException -> ApiState.NetworkError(
                    throwable,
                    throwable.message!!
                )

                else -> {
                    ApiState.GenericError(throwable.message, null)
                }
            }
        }
    }
}

private fun convertErrorBody(throwable: HttpException): BaseErrorResponse? {
    return try {
        throwable.response()?.errorBody()?.source()?.let {
            val moshiAdapter = Moshi.Builder().build().adapter(BaseErrorResponse::class.java)
            moshiAdapter.fromJson(it)
        }
    } catch (exception: Exception) {
        null
    }
}

private fun getErrorMessage(code: Int): String {
    return when (code) {
        ErrorCodes.SocketTimeOut.code -> "Timeout"
        401 -> "Unauthorised"
        404 -> "Not found"
        else -> "Something went wrong"
    }
}


