/*
 * Created by Savitha on 12/10/21, 1:20 PM
 * Copyright (c) 2021 . All rights reserved.
 * Last modified 12/10/21, 1:17 PM
 */

package com.basepackmvvm.network

import java.io.IOException

class ApiException(message: String) : IOException(message)
class NoInternetException(message: String) : IOException(message)