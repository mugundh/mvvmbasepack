/*
 * *
 *  * Created by Mugundhan on 3/17/22, 6:59 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 3/17/22, 3:24 PM
 *
 */

package com.basepackmvvm.network

import com.basepackmvvm.data.model.response.BaseErrorResponse


sealed class ApiState<out T> {
    data class Success<out T>(val data: T) : ApiState<T>()
    data class GenericError(
        val errorMessage: String? = null,
        val errorResponse: BaseErrorResponse? = null
    ) : ApiState<Nothing>()

    data class NetworkError(val exception: Exception, val errorMessage: String) :
        ApiState<Nothing>()

    object Loading : ApiState<Nothing>()
}