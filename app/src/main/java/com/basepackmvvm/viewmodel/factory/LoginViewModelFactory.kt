/*
 * *
 *  * Created by Mugundhan on 4/16/22, 6:43 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 4/16/22, 6:31 PM
 *
 */

package com.basepackmvvm.viewmodel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.basepackmvvm.data.LoginDataSource
import com.basepackmvvm.data.LoginRepository
import com.basepackmvvm.ui.activity.ui.dashboard.DashboardViewModel
import com.basepackmvvm.viewmodel.LoginViewModel

/**
 * ViewModel provider factory to instantiate LoginViewModel.
 * Required given LoginViewModel has a non-empty constructor
 */
class LoginViewModelFactory : ViewModelProvider.Factory {


    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(
                    loginRepository = LoginRepository(
                            dataSource = LoginDataSource()
                    )
            ) as T
        }
        /*else {
            return DashboardViewModel() as T
        }*/
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}