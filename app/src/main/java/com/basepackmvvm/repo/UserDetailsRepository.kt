/*
 * *
 *  * Created by Mugundhan on 4/26/22, 6:44 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 4/26/22, 4:06 PM
 *
 */

package com.basepackmvvm.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.basepackmvvm.network.ApiService
import javax.inject.Inject

class ApiRepository @Inject constructor(private val apiService: ApiService) {
    
    
    suspend fun getUserDetails(page: String) {
        try {
            val userDetails = apiService.getUserList(page)

        } catch (e: Exception) {
        }
    }

}