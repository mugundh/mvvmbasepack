/*
 * *
 *  * Created by Mugundhan on 4/15/22, 6:51 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 4/15/22, 3:42 PM
 *
 */

package com.basepackmvvm.utils

import android.app.Activity
import android.content.Context
import android.util.Log
import androidx.datastore.core.CorruptionException
import androidx.datastore.core.DataStore
import androidx.datastore.core.DataStoreFactory
import androidx.datastore.core.Serializer
import androidx.datastore.dataStore
import com.basepackmvvm.R
import com.basepackmvvm.data.model.Active
import com.basepackmvvm.data.model.User
import com.google.protobuf.InvalidProtocolBufferException
import dev.shreyaspatil.datastore.example.proto.UserPreferences
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.*


class ProtoPreferenceManager(private  var context:Activity) {

     val Context.userPreferencesStore: DataStore<UserPreferences> by dataStore(
        fileName = "PROTO_PREF",
        serializer = UserPreferenceSerializer,
    )

    val userPreference = context.userPreferencesStore.data.catch {
        if (it is IOException) {
            Log.e(TAG, "Error reading sort order preferences.", it)
            emit(UserPreferences.getDefaultInstance())
        } else {
            throw it
        }
    }.map {
        val type= it.name
        val taste = when (it.isActive) {
            UserPreferences.Active.YES -> Active.YES
            UserPreferences.Active.NO -> Active.NO
            else -> null
        }

        User(type, taste!!)
    }

    suspend fun updateUserNamePreference(name: String?,isActivePreference: Active) {

        val isActive = when (isActivePreference) {
            Active.YES -> UserPreferences.Active.YES
            Active.NO -> UserPreferences.Active.NO
        }

        context.userPreferencesStore.updateData { preferences ->
            preferences.toBuilder()
                .setName(name)
                .setIsActive(isActive)
                .build()
        }
    }
    companion object {
        const val TAG = "ProtoPreferenceManager"
    }
}

object UserPreferenceSerializer : Serializer<UserPreferences> {

    override suspend fun readFrom(input: InputStream): UserPreferences {
        try {
            return UserPreferences.parseFrom(input)
        } catch (exception: InvalidProtocolBufferException) {
            throw CorruptionException("Cannot read proto.", exception)
        }
    }

    override suspend fun writeTo(t: UserPreferences, output: OutputStream) = t.writeTo(output)

    override val defaultValue: UserPreferences
        get() = UserPreferences.getDefaultInstance()
}


/*private val dataStore: DataStore<UserPreferences> =
        DataStoreFactory.create(
            serializer = UserPreferenceSerializer,
            corruptionHandler = null,
            migrations = emptyList(),
            scope = CoroutineScope(Dispatchers.IO + Job()),
            produceFile = {
                getOutputFile()
            }
        )

    private val FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS"
    fun getOutputFile(): File {
        return File(
            getOutputDirectory(),
            SimpleDateFormat(FILENAME_FORMAT, Locale.US).format(System.currentTimeMillis())
        )
    }
   fun getOutputDirectory(): File {
        val mediaDir = context.externalMediaDirs.firstOrNull()?.let {
            File(it, context.resources.getString(R.string.app_name)).apply { mkdirs() }
        }
        return if (mediaDir != null && mediaDir.exists())
            mediaDir else context.filesDir
    }*/