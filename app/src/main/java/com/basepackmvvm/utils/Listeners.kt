package com.basepackmvvm.utils

typealias OnSuccess<T> = (T) -> Unit
typealias OnError<T> = (T) -> Unit