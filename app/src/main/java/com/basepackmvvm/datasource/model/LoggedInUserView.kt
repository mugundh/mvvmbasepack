/*
 * *
 *  * Created by Mugundhan on 3/17/22, 6:59 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 3/17/22, 3:18 PM
 *
 */

package com.basepackmvvm.datasource.model

/**
 * User details post authentication that is exposed to the UI
 */
data class LoggedInUserView(
        val displayName: String
        //... other data fields that may be accessible to the UI
)