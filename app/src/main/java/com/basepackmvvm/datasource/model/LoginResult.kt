/*
 * *
 *  * Created by Mugundhan on 17/03/22, 11:02 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 17/03/22, 10:24 PM
 *
 */

package com.basepackmvvm.datasource.model

/**
 * Authentication result : success (user details) or error message.
 */
data class LoginResult(
        val success: LoggedInUserView? = null,
        val error: Int? = null
)