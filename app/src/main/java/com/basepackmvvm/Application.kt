/*
 * *
 *  * Created by Mugundhan on 3/17/22, 6:59 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 3/17/22, 3:18 PM
 *
 */

package com.basepackmvvm

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
//import timber.log.Timber

@HiltAndroidApp
class Application : Application() {

    override fun onCreate() {
        super.onCreate()
//        if (BuildConfig.DEBUG) {
//            Timber.plant(Timber.DebugTree())
//        }
    }

}
