/*
 * *
 *  * Created by Mugundhan on 4/16/22, 6:43 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 4/16/22, 6:36 PM
 *
 */

package com.basepackmvvm.ui.activity.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.basepackmvvm.core.BaseViewModelFactory
import com.basepackmvvm.databinding.FragmentDashboardBinding
import com.basepackmvvm.viewmodel.factory.LoginViewModelFactory
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DashboardFragment : Fragment() {

    private var _binding: FragmentDashboardBinding? = null
    private val binding get() = _binding!!

    private val dashboardViewModel by viewModels<DashboardViewModel>(factoryProducer = {
        BaseViewModelFactory()
    })

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val textView: TextView = binding.textDashboard
        dashboardViewModel.text.observe(viewLifecycleOwner) {
            textView.text = it
        }
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}