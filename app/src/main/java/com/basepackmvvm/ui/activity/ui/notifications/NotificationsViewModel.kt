/*
 * *
 *  * Created by Mugundhan on 4/16/22, 6:43 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 4/16/22, 11:08 AM
 *
 */

package com.basepackmvvm.ui.activity.ui.notifications

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class NotificationsViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is notifications Fragment"
    }
    val text: LiveData<String> = _text
}