/*
 * *
 *  * Created by Mugundhan on 4/16/22, 6:43 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 4/16/22, 6:22 PM
 *
 */

package com.basepackmvvm.ui.activity.ui.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DashboardViewModel @Inject constructor(): ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is dashboard Fragment"
    }
    val text: LiveData<String> = _text


}