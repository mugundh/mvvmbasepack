/*
 * *
 *  * Created by Mugundhan on 4/15/22, 6:51 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 4/15/22, 6:07 PM
 *
 */

package com.basepackmvvm.data

import com.basepackmvvm.data.model.LoggedInUser
import java.io.IOException
import javax.inject.Inject

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource  @Inject constructor() {

    fun login(username: String, password: String): Result<LoggedInUser> {
        try {
            val fakeUser = LoggedInUser(java.util.UUID.randomUUID().toString(), "Jane Doe")
            return Result.Success(fakeUser)
        } catch (e: Throwable) {
            return Result.Error(IOException("Error logging in", e))
        }
    }

    fun logout() {
        // TODO: revoke authentication
    }
}