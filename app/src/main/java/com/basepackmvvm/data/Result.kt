/*
 * *
 *  * Created by Mugundhan on 3/17/22, 6:59 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 3/17/22, 3:18 PM
 *
 */

package com.basepackmvvm.data

/**
 * A generic class that holds a value with its loading status.
 * @param <T>
 */
sealed class Result<out T : Any> {

    data class Success<out T : Any>(val data: T) : Result<T>()
    data class Error(val exception: Exception) : Result<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$exception]"
        }
    }
}