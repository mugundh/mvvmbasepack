/*
 * *
 *  * Created by Mugundhan on 3/17/22, 6:59 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 3/17/22, 3:24 PM
 *
 */

package com.basepackmvvm.data.model.response

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BaseErrorResponse(

    @Json(name = "success")
    val success: Boolean,

    @Json(name = "message")
    val message: String,
)
