/*
 * *
 *  * Created by Mugundhan on 4/13/22, 1:34 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 4/12/22, 1:42 PM
 *
 */

package com.basepackmvvm.data.model

data class User(
    val name: String,
    val type: Active
)

enum class Active {
    YES, NO
}
