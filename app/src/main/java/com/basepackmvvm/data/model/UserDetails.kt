/*
 * *
 *  * Created by Mugundhan on 3/17/22, 6:59 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 3/17/22, 3:18 PM
 *
 */

package com.basepackmvvm.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserDetails(
    val user: String? = "",
    val avatar: String? = "",
    val name: String? = "",
    val userSince: String? = "",
    val location: String? = ""
) : Parcelable