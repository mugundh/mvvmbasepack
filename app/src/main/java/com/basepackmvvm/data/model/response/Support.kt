/*
 * *
 *  * Created by Mugundhan on 4/26/22, 6:44 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 4/25/22, 3:18 PM
 *
 */

package com.basepackmvvm.data.model.response


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Support(
    @Json(name = "text")
    val text: String,
    @Json(name = "url")
    val url: String
)