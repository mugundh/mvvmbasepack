/*
 * *
 *  * Created by Mugundhan on 4/26/22, 6:44 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 4/25/22, 3:18 PM
 *
 */

package com.basepackmvvm.data.model.response


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SampleUser(
    @Json(name = "data")
    val `data`: List<Data>,
    @Json(name = "page")
    val page: Int,
    @Json(name = "per_page")
    val perPage: Int,
    @Json(name = "support")
    val support: Support,
    @Json(name = "total")
    val total: Int,
    @Json(name = "total_pages")
    val totalPages: Int
)