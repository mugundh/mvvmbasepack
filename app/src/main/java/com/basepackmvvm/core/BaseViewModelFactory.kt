/*
 * *
 *  * Created by Mugundhan on 4/26/22, 6:44 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 4/26/22, 12:43 PM
 *
 */

package com.basepackmvvm.core

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.basepackmvvm.data.LoginDataSource
import com.basepackmvvm.data.LoginRepository
import com.basepackmvvm.ui.activity.ui.dashboard.DashboardViewModel
import com.basepackmvvm.ui.activity.ui.home.HomeViewModel
import com.basepackmvvm.viewmodel.LoginViewModel

/**
 * ViewModel provider factory to instantiate LoginViewModel.
 * Required given LoginViewModel has a non-empty constructor
 */
class BaseViewModelFactory : ViewModelProvider.Factory {


    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(
                    loginRepository = LoginRepository(
                            dataSource = LoginDataSource()
                    )
            ) as T
        }
        else if (modelClass.isAssignableFrom(DashboardViewModel::class.java)){
            return DashboardViewModel() as T
        }
        else if (modelClass.isAssignableFrom(HomeViewModel::class.java)){
            return HomeViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}